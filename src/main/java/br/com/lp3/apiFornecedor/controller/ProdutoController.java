package br.com.lp3.apiFornecedor.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.lp3.apiFornecedor.dto.ProdutoDTO;
import br.com.lp3.apiFornecedor.service.ProdutoService;



@RestController
@RequestMapping(value = "/produto")
public class ProdutoController {
	
	
	

		@Autowired
		private ProdutoService produtoService;
		
		@RequestMapping(value="/{id}", method = RequestMethod.GET)
		public ResponseEntity<ProdutoDTO> findClienteById(@PathVariable Long id) {
			ProdutoDTO produtoDTO = produtoService.findById(id);
			if (produtoDTO != null) {
				return new ResponseEntity<ProdutoDTO>(produtoDTO, HttpStatus.OK);
			} else {
				return new ResponseEntity<ProdutoDTO>(produtoDTO, HttpStatus.NOT_FOUND);
			}
		}
		
		@RequestMapping(value="", method = RequestMethod.POST)
		public ResponseEntity<ProdutoDTO> saveProduto(@RequestBody ProdutoDTO produtoDTO) {
			produtoDTO = produtoService.saveProduto(produtoDTO);
			if (produtoDTO != null) {
				return new ResponseEntity<ProdutoDTO>(produtoDTO, HttpStatus.OK);
			} else {
				return new ResponseEntity<ProdutoDTO>(produtoDTO, HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
			
			
			@RequestMapping(value="delete", method = RequestMethod.POST)
			public ResponseEntity<ProdutoDTO> deletarProduto(@RequestBody ProdutoDTO produtoDTO) {
				produtoDTO = produtoService.deletarProduto(produtoDTO);
				if (produtoDTO != null) {
					return new ResponseEntity<ProdutoDTO>(produtoDTO, HttpStatus.OK);
				} else {
					return new ResponseEntity<ProdutoDTO>(produtoDTO, HttpStatus.NOT_FOUND);
				}
				
				
				
			
		}
		
	

}
